require 'base64'

Before do
    page.current_window.resize_to(1440,900)

    @order_page = OrderPage.new
    @rest_page = RestaurantPage.new #variavel de instancia.fica disponivel durante a execução do cenario.
    @rest_list_page = RestaurantListPage.new
end
#cria um gancho
Before('@bread_bakery') do
    visit '/restaurants/bread-bakery/menu'
end

Before('@green_food') do
    visit 'restaurants/green-food/menu'
end

After do |scenario|
    if scenario.failed?
        shot_file = page.save_screenshot('log/screenshot.png')
        shot_b64 = Base64.encode64(File.open(shot_file, "rb").read) #vai ler o arquivo que é uma imagem, converte para b64 e guarda na variavel
        embed(shot_b64, 'image/png', 'Screenshot') #cucumber anexa o screenshot no report
    end
end