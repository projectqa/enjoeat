Dado("que eu fechei o pedido com os itens:") do |table|
    @product_list = table.hashes #array de itens
    steps %{
        Quando eu adiciono todos os itens
    }
    @rest_page.cart.close
end
  
Dado("informei os meus dados de entrega:") do |table|
    user = table.rows_hash #chave e valor, uma linha. um objeto de chave e valor.
    @order_page.fill_user_data(user)
    
end
  
Quando("eu finalizo o pedido com {string}") do |payment|
    @order_page.checkout(payment)
end
  
Então("deve ver a seguinte mensagem:") do |expect_message|
    expect(@order_page.summary).to have_text expect_message
end