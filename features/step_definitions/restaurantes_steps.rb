# Dado("que tenho uma lista de restaurantes") do

#     @restaurant_data = [
#         { name: 'Bread & Bakery', category: 'Padaria', delivery_time: '25 minutos', rating: 4.9},
#         { name: 'Burger House', category: 'Hamburgers', delivery_time: '30 minutos', rating: 3.5},
#         { name: 'Coffee Corner', category: 'Cafeteria', delivery_time: '20 minutos', rating: 4.8}
#     ]
#   end

# Quando("acesso a lista de restaurantes") do
#     visit '/restaurants'
# end
  
# Então("vejo todas as opções disponíveis") do
#     restaurantes = all('.restaurant-item') # o metodo devolde uma coleção de elementos (array)
#     expect(restaurantes.size).to be > 0
# end
  
# Então("cada restaurante deve exibir sua categoria") do

#     #a massa de teste deve ta na mesma sequencia das informações exibidas na pagina
#     restaurants = all('.restaurant-item')

#     @restaurant_data.each_with_index do |value, index|

#         #espero que restaurantes na mesma posicao da minha massa de test, contem a categoria
#         expect(restaurants[index]).to have_text value[:category] 
#     end
# end
  
# Então("cada restaurante deve exibir o tempo de entrega") do

#     #a massa de teste deve ta na mesma sequencia das informações exibidas na pagina
#     restaurants = all('.restaurant-item')

#     @restaurant_data.each_with_index do |value, index|

#         #espero que restaurantes na mesma posicao da minha massa de test, contem a categoria
#         expect(restaurants[index]).to have_text value[:delivery_time] 
#     end
# end
  
# Então("cada restaurante deve exibir sua nota de avaliação") do

#     #a massa de teste deve ta na mesma sequencia das informações exibidas na pagina
#     restaurants = all('.restaurant-item')

#     @restaurant_data.each_with_index do |value, index|

#         #espero que restaurantes na mesma posicao da minha massa de test, contem a categoria
#         expect(restaurants[index]).to have_text value[:rating] 
#     end
# end


# Então("cada restaurante deve ter {int} {string} {string} {string} {float}") do |id, name, category , delivery_time, rating|
    
#     restaurants = all('.restaurant-item')

#     expect(restaurants[id]).to have_text name.upcase
#     expect(restaurants[id]).to have_text category
#     expect(restaurants[id]).to have_text delivery_time
#     expect(restaurants[id]).to have_text rating
#   end

Dado("que temos os seguintes restaurantes") do |table|
    @restaurant_data = table.hashes
end
  
Quando("acesso a lista de restaurantes") do
    @rest_list_page.load
end
  
Então("devo ver todos os restaurantes desta lista") do
   
    restaurants = @rest_list_page.list

    @restaurant_data.each.with_index do |value, index|
        expect(restaurants[index]).to have_text value['nome'].upcase
        expect(restaurants[index]).to have_text value['categoria']
        expect(restaurants[index]).to have_text value['entrega']
        expect(restaurants[index]).to have_text value['avaliacao']
    end
end