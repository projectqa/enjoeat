#language: pt

@green_food
Funcionalidade: Finalizar pedido
    Para que eu possa receber o pedido no meu endereço
    Sendo um usuário que fechou o carrinho com itens
    Posso finalizar o meu pedido

    # BDD (Desenvolvimento orientado a comportamento) => voltado para o negócio
    # BDT (Teste guiado por comportamento)

    # sai do modelo tradicional, para o modelo que tem valor ao negocio
    # deixa de somente achar bugs, passa a ser estratégico

    # Cenario: Finalizar pedido com Cartão Refeição

    #     Dado que eu fechei o meu carrinho
    #     E preencho o campo nome com "Fernando"
    #     E preencho o campo email com "fernando@hotmail.com"
    #     E preencho o campo confirmação de email com "fernando@hotmail.com"
    #     E preencho a rua com "Avenida Paulista"
    #     E preencho o numero da rua com "1000"
    #     E preencho o campo complemento com "17 andar"
    #     E clico na forma de pagamento "Cartão Refeição"
    #     Quando eu clico em "Finalizar Pedido"
    #     Então devo ver uma mensagem de sucesso

#test de fumaça, smoke test, um dos testes mais importantes para minha aplicação.
    @smoke
    Cenario: Finalizar pedido com Cartão Refeição

        Dado que eu fechei o pedido com os itens:
            | quantidade | nome                 | descricao                               | subtotal |
            | 1          | Suco Detox           | Suco de couve, cenoura, salsinha e maçã.| R$ 14,90 |
            | 2          | Hamburger de Quinoa | Cheio de fibras e muito saboroso.        | R$ 21,00 |
        E informei os meus dados de entrega:
            | nome        | Fernando         |
            | email       | eu@papito.io     |
            | rua         | Avenida Paulista |
            | numero      | 1000             |
            | complemento | 17o andar        |
        Quando eu finalizo o pedido com "Cartão Refeição"
        Então deve ver a seguinte mensagem:
        #Doc String. Quando a mensagem é mto grande.
        """
        Seu pedido foi recebido pelo restaurante. Prepare a mesa que a comida está chegando!
        """